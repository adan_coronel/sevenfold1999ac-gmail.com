!!(wanhui 2003.11.05)
!!-------------------

#include <misc.h>
#include <params.h>

#if (defined SPMD)

      integer nprocessor
#if defined COUP_CSM
      parameter(nprocessor=NTASK)            !by LPF
#else
      parameter(nprocessor=8)
#endif
      integer,parameter :: nx = PLON+2
      integer,parameter :: ny = PLAT/nprocessor+1+2
      integer,parameter :: nl = 26
      integer,parameter :: nz = 27

#else
      integer,parameter :: nx = PLON+2
      integer,parameter :: ny = PLAT
      integer,parameter :: nl = 26
      integer,parameter :: nz = 27
#endif

