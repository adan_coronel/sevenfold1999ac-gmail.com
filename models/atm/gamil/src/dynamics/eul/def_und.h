#ifndef DEF_UND_SET
#define DEF_UND_SET
#define XLON 81
#define YLAT 161
#define ZETA 20
#define IBG  501
#define JBG  701
#define WBG  50
#define NBG  70
#define LB   10
#define XBGP 360
#define YBGP 181
#define ZBGL  21
#define DLAT 0.05
#define DLON 0.05
#define LEV925MB
#define NCP2_DAT
#define SPLINE_INTP
#define NO_V_BOUNDARY
#define NO_GETB
#define NO_MOUNTAIN
#define  ETA_PURE
#undef   SIGMA
#undef  SIGMA_ETA
#undef  CONV_BETTS
#undef  P_BBOUNDARY_L
#undef  GRAV_DRAG
#undef  RADIATION
#undef  CLOUD_F
#undef  MON_OUT
#undef  SP_OUT
#undef  RESTART_OUT
#define EXTRE_OUT
#undef  PBL_CCM3
#undef  FLX_BIAM
#undef  RAD_MM5
#undef  CLD_BIAM
#undef  MDF_PBL0
#undef  PHYSICS
#define NESTED_NO
#undef  CLD_OUT
#undef  FLX_OUT
#define SST_VAR_NOT
#define SST_DAI_NOT
#define SST_HOUR_NOT
#define NO_TC_ITER
#endif
