#include <params.h>

integer,parameter :: COMM_TO_LEFT = 0
integer,parameter :: COMM_TO_RIGHT = 1
integer,parameter :: COMM_TO_TOP = 2
integer,parameter :: COMM_TO_BOT = 3
integer,parameter :: COMM_ROTATE_LEFT = 4
integer,parameter :: COMM_ROTATE_RIGHT = 5
integer,parameter :: PACK_FOR_PHYS = 1
integer,parameter :: PACK_FOR_1D = 2

integer,parameter :: XLON = PLON+2
integer,parameter :: YLAT = PLAT
integer,parameter :: ZALT = 27
